//on lance les jobs sur le slave
node('slave') {

    //on regarde sur quelle branche on se trouve
    checkout scm 
    
    //on definit une variable par rapport à la branche
    def envi = env.BRANCH_NAME
    
    //on definit la variable en test ou prod en fonction de la branche
    stage('on interroge la branche') {
        if (env.BRANCH_NAME == 'master') {
            envi = "prod"
        } else {
            envi = "test"
        }
    }

    //dans une image docker
    docker.image('raphaelseguin/terraform-ansible-git:2.4').inside() {
    
        //on recupere le git pour avoir nos fichiers Terraform    
        
        stage('git des fichiers Terraform dans une image Docker') {
            git url: 'https://gitlab.com/projet_final1/pile.git'
        }
        
        //on place les fichiers terraform dans le repertoire de travail
        
        stage('on copie les fichiers terraform dans le repertoire de travail'){
            sh "mv ./terraform/* ."
        }
        
        //on init et plan terraform
        
        stage('on initie et plan Terraform'){
        
                //on passe les fichiers de credential
                
            withCredentials([file(credentialsId: 'backend.tfvars', variable: 'test')]) {
                
                //on init terraform
                sh "terraform init"
                
                //on plan terraform
                sh "terraform plan -var 'env=${envi}' -var-file=\$test -out planfile"
            }
        }
        
        //on deploie terraform
        
        stage('on applique terraform'){
            sh "terraform apply planfile"
        }
    }
    
    //on va chercher la bonne branche du projet
    stage('on va chercher le git du projet'){       
        git branch: env.BRANCH_NAME, url: 'https://gitlab.com/projet_final1/Projet-final_Restful-Webservice.git'
    }
    
    //on clean avec Maven
    stage('on clean avec maven') {
        sh "mvn clean"
    }
    
    //on déclanche le build et les test MAven en parallèle avec un failfast activé
    parallel(
      test: { sh "mvn test" },
      
      build: { sh "mvn package" },
      failFast: true
    )
    
    //on publie les tests du package
    stage('on teste le jar') {
            junit '**/target/surefire-reports/TEST-*.xml'
            archiveArtifacts 'target/*.jar'
    }
    //on push l artifact sur le serveur
    stage('on push le jar sur le serveur') {
        withCredentials([sshUserPrivateKey(credentialsId: 'slave', keyFileVariable: 'key', passphraseVariable: '', usernameVariable: 'jpr')]) {
            sh "scp -i \$key -o StrictHostKeyChecking=no target/restfulweb-1.0.0-SNAPSHOT.jar jpr@jpr-devops-${envi}.francecentral.cloudapp.azure.com:/home/jpr"
        }
    }
    
    //on deploie la config avec ansible dans une image Docker
    docker.image('raphaelseguin/terraform-ansible-git:2.4').inside("--net=host -u root") {
    
        //on va rechercher le git du projet pour obtenir les fichiers de config
        stage('on va chercher le git du projet'){       
            git url: 'https://gitlab.com/projet_final1/pile.git'
        }

        //on prepare les credentials
        withCredentials([sshUserPrivateKey(credentialsId: 'slave', keyFileVariable: 'key', passphraseVariable: '', usernameVariable: 'jpr')]) {
            sh "cat \$key > ~/.ssh/jprkey"
            sh "chmod 400  ~/.ssh/jprkey"
        }

        //on deploie avec Ansible
        stage('Deploiement Ansible') {
            
                ansiblePlaybook (
                    colorized: true, 
                    become: true,
                    playbook: "ansible/playbook-${envi}.yml",
                    inventory: 'ansible/inventory.ini',
                    credentialsId: 'slave'
                    )
            
        }
    }
}